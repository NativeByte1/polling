var mongoose=require('mongoose');
mongoose.connect('mongodb://localhost:27017/pollo',{useNewUrlParser:true, useCreateIndex:true,});
var conn=mongoose.Collection;

var userSchema=new mongoose.Schema({
  username: {
    type:String,
    required:true,
    index:{
      unique:true,
    }
  },

  emailid: {
    type:String,
    required:true,
    index:{
      unique:true,
    }
  },

  password: {
    type:String,
    required:true,
  },

  date:{
    type:Date,
    default:Date.now
  }
});

var userModel=mongoose.model('users',userSchema);
module.exports=userModel;
